# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [v0.1.8] - 2018-06-06

### Changed

- Moved to GitLab

## [v0.1.7] - 2018-05-14

### Fixed

- Updated clock limits to apply to the F0x0 chips

## [v0.1.6] - 2018-04-20

### Added

- TIM14 through TIM17 timers

## [v0.1.5] - 2018-04-20

### Added

- APB2 has returned, huzzah!

## [v0.1.4] - 2018-04-20

### Added

- Methods to switch pins into `AF0` and `AF1` modes

## [v0.1.3] - 2018-04-19

### Added

- Implemented `InputPin` for pins in open-drain output mode (behind a flag, since it's a pretty ugly hack)

## [v0.1.2] - 2018-04-19

### Added

- Implemented `InputPin` for pins input mode

## [v0.1.1] - 2018-04-13

### Fixed

- A few places still referred to stm32f30x

## v0.1.0 - 2018-04-13

Initial release
